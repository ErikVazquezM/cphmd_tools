from cphmd_tools import cphmdanalysis
from pathlib import Path
path = Path(__file__).parent / "../examples/example_data/"

single_ph_strings = ['3.30', '4.30', '5.30', '6.30', '7.30', '8.30', '9.30']
single_phs = [float(ph) for ph in single_ph_strings]
rex_ph_strings = ['3.0', '3.5', '4', '4.5', '5', '5.5', '6', '6.5', '7', '7.5', '8', '8.5']
rex_phs = [float(ph) for ph in rex_ph_strings]


lambda_files = [cphmdanalysis.lambda_file('{}/single_ph_lambdas/1eh6_{}.lambda'.format(path, ph))
                for ph in single_ph_strings]

for lambda_file in lambda_files:
    lambda_file.compute_s_values()


pka_data = cphmdanalysis.compute_pkas(single_phs, lambda_files)

with open('{}/test_results/test_single_pKa.txt'.format(Path(__file__).parent), 'w+') as sf:
    sf.write('#Residue Hill pKa\n')
    for i, resid in enumerate(lambda_files[0].titrreses):
        sf.write('{} {:.2f} {:.1f}\n'.format(resid, pka_data[i][0], pka_data[i][2]))


replica_lambda_files = [cphmdanalysis.lambda_file('{}/rex_lambdas/1eh6{}.lambda'.format(path, ph))
                        for ph in rex_ph_strings]

rem_file = cphmdanalysis.rem_file('{}/rex_lambdas/1eh6_rem.log'.format(path), 1000)
wrapped_lambda_files = cphmdanalysis.wrap_lambda_files(rem_file, replica_lambda_files, rex_phs)
pka_data = cphmdanalysis.compute_pkas(rex_phs, wrapped_lambda_files)

with open('{}/test_results/test_REX_pKa.txt'.format(Path(__file__).parent), 'w+') as rf:
    rf.write('#Residue Hill pKa\n')
    for i, resid in enumerate(wrapped_lambda_files[0].titrreses):
        rf.write('{} {:.2f} {:.1f}\n'.format(resid, pka_data[i][0], pka_data[i][2]))

True
