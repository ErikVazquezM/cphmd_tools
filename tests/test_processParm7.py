from cphmd_tools.processParm7 import processParm7
from pathlib import Path
path = Path(__file__).parent / "../examples/example_data/"

models = ['LYS', 'CYS', 'HIP', 'GL2', 'AS2']
for model in models:
    pdb = '{}/parm7/{}_model.pdb'.format(path, model)
    parm7 = '{}/parm7/{}_model.parm7'.format(path, model)
    newparm7 = 'tests/test_results/cphmd_{}_model.parm7'.format(model)
    processParm7(pdbFile=pdb, parm7File=parm7, newParm7File=newparm7)
