{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Compute the pKa's from a set of single-pH simulations"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "One way to use the CpHMD method to obtain the pKa's of residues in a protein is to choose a set of pH values over the pH-range of interest and run separate simulations at each pH. You can then extract the fraction of the time that each residue was deprotonated at each pH and fit these values the Henderson-Hasselbalch equation.\n",
    "\n",
    "Alternatively, one can use pH replica-exchange to accelerate the calculations. Like in the single-pH simulations, multiple simulations are run at different pH's, but in the replica-exchange methods these simulations periodically attempt to swap pH states, potentially allowing the system to escape local free energy minima.\n",
    "\n",
    "The notebook cphmdanalysis_replica.ipynb provides an example of how to analyze the results from a replica-exchange run in Amber.\n",
    "\n",
    "Although the importance-sampling provided by replica exchange makes that method more attractive than single-pH calculations in most circumstances, single-pH calculations can be useful in resource-constrained environments. For example, in replica exchange on GPUs in Amber, one GPU is required for each replica. Obtaining several connected GPUs to run such a calculation may not always be possible."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Import library"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First, import the library to give access to the cphmd commands plus the plotting libraries so that we can view our data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [],
   "source": [
    "from cphmd_tools import cphmdanalysis\n",
    "import matplotlib\n",
    "import matplotlib.pyplot as plt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Define constants"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here I define a list of pH's at which the job was run.\n",
    "\n",
    "I define a list of strings as well as floats so that I can more easily specify the file paths later, but in general this is not necessary.\n",
    "\n",
    "This example was taken from a project where we were interested in the pKa of a particular Cys residue, whose experimental pKa was 5.3. I therefore ran one simulation at that pKa and other simulations at intervals of 1 pH unit.\n",
    "\n",
    "In most circumstances, you won't know what the pKa of your residue of interest is or you will be interested in the pKa's of multiple residues. In those cases, I would recommend using simpler pH values, such as whole units or half units.\n",
    "\n",
    "Setting the spacing between pH values can be very important. The titration curves from single-pH simulations can be noisy, and sometimes you will need to add additional windows to obtain a well-fit curve."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "single_ph_strings = ['3.30', '4.30', '5.30', '6.30', '7.30', '8.30', '9.30']\n",
    "single_phs = [float(ph) for ph in single_ph_strings]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Read lambda files"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The first step is to read in all of the lambda files generated from the simulations. In example_data/single_ph_lambdas we have the lambda files generated from a set of single-pH simulations. This command creates a list of lambda_file objects, which each store all of the information from the lambda files so it can easily be accessed through the members of the objects. These objects are then used by the other functions to calculate the pKa's.\n",
    "\n",
    "For a detailed definition of the lambda_file objects and a list of all members, check the detailed documentation in cphmdanalysis.py."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "None\n"
     ]
    }
   ],
   "source": [
    "print(cphmdanalysis.henderson_hasselbalch_for_fitting.__doc__)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "lambda_files = [cphmdanalysis.lambda_file('example_data/single_ph_lambdas/1eh6_{}.lambda'.format(ph))\n",
    "                for ph in single_ph_strings]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Compute S values"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, we need to compute the unprotonated fractions so that we can fit to the Henderson-Hasselbalch equation. This is done with the function compute_s_values."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "for lambda_file in lambda_files:\n",
    "    lambda_file.compute_s_values()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Compute pKa's"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, compute the pKa's of the titratable residues. compute_pkas returns a list of lists containing one list for each titratable residue that in turn contains in order the Hill coefficient, the fitting error in the Hill coefficient, the pKa, and the fitting error in pKa.\n",
    "\n",
    "Because the Henderson-Hasselbalch equation is nonlinear, we have to make initial guesses for the Hill coefficient and pKa. The algorithm here guesses that the Hill coefficient is 1. It takes as the initial guess of the pKa the pH value the unprotonated fraction is closest to 0.5, unless none of the unprotonated fractions are between 0.1 and 0.9. If there is no appropriate pH to pick as an initial guess or if the fitting function fails to converge, the algorithm will return math.nan for the pKa data for that residue.\n",
    "\n",
    "This function does not throw errors when the fitting fails because often there will be residues that do not titrate in the pH range, and if these residues are not ones you are interested in it is usually not a problem. Later in the notebook, I will show how to look more closely at these cases."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "pka_data = cphmdanalysis.compute_pkas(single_phs, lambda_files)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Print pKa data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, print the pKa estimates and Hill values. Note that the titrreses directory in the lambda_file objects contains the residue indices of the titratable residues. This means, if, for example, we want to know what the pKa of a particular residue is, we can pull it out of the pKa data using this array, as is shown in the next cell."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Residue Hill pKa\n",
      "2 2.08 6.6\n",
      "3 0.72 3.7\n",
      "5 nan nan\n",
      "10 0.69 3.4\n",
      "15 nan nan\n",
      "17 0.66 4.0\n",
      "21 0.38 5.6\n",
      "22 0.57 3.9\n",
      "26 0.67 6.3\n",
      "27 0.40 2.6\n",
      "29 nan nan\n",
      "33 nan nan\n",
      "39 0.62 3.6\n",
      "42 0.68 4.5\n",
      "54 0.60 4.1\n",
      "59 nan nan\n",
      "68 0.77 7.4\n",
      "71 0.62 3.8\n",
      "74 0.84 4.7\n",
      "75 0.82 4.4\n",
      "82 0.75 6.3\n",
      "83 1.65 3.2\n",
      "89 0.86 3.7\n",
      "98 1.00 9.9\n",
      "101 nan nan\n",
      "104 nan nan\n",
      "107 0.87 3.5\n",
      "122 0.96 10.2\n",
      "142 0.27 5.9\n",
      "143 0.41 3.7\n",
      "147 nan nan\n",
      "162 0.52 10.9\n",
      "163 0.86 4.4\n",
      "168 0.95 7.4\n",
      "169 0.63 3.8\n",
      "171 0.78 6.3\n"
     ]
    }
   ],
   "source": [
    "print('Residue Hill pKa')\n",
    "for i, resid in enumerate(lambda_files[0].titrreses):\n",
    "    print('{} {:.2f} {:.1f}'.format(resid, pka_data[i][0], pka_data[i][2]))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Say that we are interested specifically in the pKa of residue 142, which was the Cys residue we were interested in. We can use the titrreses array in the lambda_file object to pull it out of the pka_data object:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "pKa of residue 142 = 5.9\n"
     ]
    }
   ],
   "source": [
    "residue = 142\n",
    "print('pKa of residue 142 = {:.1f}'.format(pka_data[lambda_files[0].titrreses.index(residue)][2]))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can check to see why a residue did not return a pKa by looking directly at the unprotonated fractions:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 26,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAjgAAAEYCAYAAABRMYxdAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAADh0RVh0U29mdHdhcmUAbWF0cGxvdGxpYiB2ZXJzaW9uMy4xLjAsIGh0dHA6Ly9tYXRwbG90bGliLm9yZy+17YcXAAAgAElEQVR4nO3de5wcdZ3v/9c7k8sECJcQWEICIvdDWAwQNAHFQFBBEVbl/IRVwbOy6P5EOahnJepZUVT04GN1vXIQkQA+YP1FwCzKzZDIbZCbGAk3w0WJJCSBEBKSMMnM5/dH1QydYbqnqqYr093zfj4e9Ziu7ppvf7r7nZpPqqqrFBGYmZmZtZIRQ12AmZmZWb25wTEzM7OW4wbHzMzMWo4bHDMzM2s5bnDMzMys5bjBMTMzs5bjBmcrkLRY0swqj82UtHQrl2QNxhmxvJwZK2I45cYNTgVJz0jaIGmdpOWSLpe03WDHjYgpEbGwDjWtk3TLYOux4ho0IxdI+pOkzZLO7/PYTEndFflZJ+mMwdZr2TVaZiTtKulqSc9JWiPpLklvqXhckr4o6a+SXpZ0jaTtB1uv5dNouUlrWiBpZZqLP0o6ueKxY9L10EuSXpB0naRJg613MNzgvN57I2I7YCpwKDB7iOuBtKZ0eudQF2MNl5ElwL8Cv67y+HMV+dkuIuZsxdos0UiZ2Q64DzgcGA/MAX5d8cfzdOAjwFHA7sBY4PtDUKc1Vm4AzgEmRsT2wFnAVZImpo89ArwrInYkyc2fgR8PTZkJNzhVRMRy4GaSYAEgaYykb6f/s3le0sWSxqaPTZB0Q9q9vijpDkkj0seekXRcents2omvlvQIcMQQvDyrg0bJSETMiYgbgbVlvVarj0bITEQ8FRH/HhHLIqIrIi4BRgMHpIu8F/hpRDwbEeuAbwEflLRNCW+JZdAIuUnrWBQRm3tmgVHAHuljz0fEcxWLdwH71ucdKMYNThWSJgMnkPzvuMe3gP1JQrYvMAn4t/SxzwJLgV2AvwO+QBKAvr4M7JNO7wKy7C74ebpZ8BZJb8r/aqwMDZaRWnZNV4BPS/qOpG0HOZ4V1IiZkTSVpMHpqUnpRMX8GGC/rGNafTVSbtLGaSPwe2AhcH/FY3tKegnYAHwO+D9ZX2MpIsJTOgHPAOtI/iccwHxgx/QxAa8A+1QsPwN4Or39VeBXwL5Vxj0uvf0UcHzFY2cBS2vUdBTJJuJtSDZPLu+pyZMz0meMq4Dz+9y3G3AQyX9m3gjcDvzfoX4fh9PU4JnZHvgTMLvivjOBJ4C9gB2AeWndM4b6vRxOU4PnZhRJw3VulcfHA58Hpg/le+gtOK/3DxExDpgJHAhMSO/fhaTJeCDd7PcScFN6P8BFJN31LZKeknRelfF3B56tmP9LrWIi4q6I2BAR6yPiQuAl4G0FXpfVT0NlpJaIWB4Rj0REd0Q8TXKszilFx7PCGi4z6e6M/wLuSdctPS4Drib53/liYEF6f8t8u6aJNFxuACJiUyS7xd8l6aR+Hn+R5NiuX0kamWXMMrjBqSIifgdcDnw7vWsVyWa3KRGxYzrtEMkBYETE2oj4bETsTbIP+zOSZvUz9DLSfZapPfOWxpabj22INHBGapaN8zNkGiUzksYA1wN/Az7ep8buiPhyROwVEZNJmpy/pZMNgUbJTT9GkuzeqvbYriRbCYeEG5zavgu8Q9LUiOgGfgJ8R9KuAJImSXpXevtESftKEvAyyQFWXf2M+QtgtqSd0v2qn6r25On+zKMkjZbULul/kXTwd9X1VdpgDGlG0nFHSWon+fc8Ms1KW/rYzDRHkrQH8E2STdc2dIZ6vTIKmEvyB/L0tIbKx8dL2ifNzEHAvwNf7bucbXVDnZsDJZ2QHpg8StKHgaOB36WPv1/SAZJGSNqFJDd/SLfmDAk3ODVExErgCuB/p3d9nmSz3z2SXgZ+y2vfPNgvnV8HdAA/iv7PNfAVks2ATwO3AFfWKGEcydfsVpP87+l44ISIeKH4q7J6aoCMQLKi2wCcBnwxvf2R9LHD0ud6BbgbeBj4dJ7XaPXVAJk5EjgReCfwkl47P1LPru8JwG9IMnMjcFkk37SyIdQAuRFwPrACWEnylfEPRsSD6eOTSHaTrSU5rqsbeF/Ol1lXSg8IMjMzM2sZ3oJjZmZmLae0BkfSZZJWSHq4yuOS9D1JSyQtknRYWbVY83BuLC9nxvJyZoaHMrfgXE5yzEg1J5DsJ9yP5Lv3Q3pKZ2sYl+PcWD6X48xYPpfjzLS80hqciLgdqHX09MnAFZG4B9hRr13TwoYp58bycmYsL2dmeBjKY3AmseUJhpam972OpLMk3S/p/ilTpgTJuTw81X9qBply48w4NxW8rmm8qdE5M4035TaUDU5/Jxvr90VExCURMS0ipo0dO7bksqzBZcqNM2MVvK6xvJyZFjCUDc5StjyD4mTguSrLmvVwbiwvZ8bycmZawFA2OPOA09Oj1acDayJi2RDWY83BubG8nBnLy5lpAaVdBEvS1SQXCJsgaSnJZdlHAUTExSRnynw3yZkY1wP/o6xarHk4N5aXM2N5OTPDQ2kNTkScNsDjAXyyrOe35uTcWF7OjOXlzAwPPpOxmZmZtRw3OGZmZtZy3OCYmZlZy3GDY2ZmZi3HDY6ZmZm1HDc4ZmZm1nLc4JiZmVnLcYNjZmZmLWfABkfSrZJ2rJjfSdLN5ZZlzcyZsSKcG8vLmbFasmzBmRARL/XMRMRqYNfySrIW4MxYEc6N5eXMWFVZGpxuSXv2zEh6A1UuG2+WcmasCOfG8nJmrKos16L6InCnpN+l80cDZ5VXkrUAZ8aKcG4sL2fGqhqwwYmImyQdBkwHBJwbEatKr8yaljNjRTg3lpczY7VkvZp4F7ACaAcOkkRE3F5eWdYCnBkrwrmxvJwZ69eADY6kM4FzgMnAQySdcgdwbLmlWbNyZqwI58bycmasliwHGZ8DHAH8JSKOAQ4FVpZalTU7Z8aKcG4sL2fGqsrS4GyMiI0AksZExGPAAeWWZU3OmbEinBvLy5mxqrIcg7M0PZHS9cCtklYDz5VbljU5Z8aKcG4sL2fGqsryLar3pTfPl7QA2AG4qdSqrKk5M1aEc2N5OTNWS80GR9IIYFFEHAwQEb+rtbyZM2NFODeWlzNjA6l5DE5EdAN/rDxTpFktzowV4dxYXs6MDSTLMTgTgcWS7gVe6bkzIk4qrSprds6MFeHcWF7OjFWVpcH5SulVWKtxZqwI58bycmasqqoNjqTpEXGP92taVs6MFeHcWF7OjGVR6xicH/XckNSxFWqx5ufMWBHOjeXlzNiAajU4qrjdXnYh1hKcGSvCubG8nBkbUK1jcEZI2omkCeq53RuqiHix7OKs6TgzVoRzY3k5MzagWg3ODsADvBaaByseC2DvsoqypuXMWBHOjeXlzNiAqjY4EbHXVqzDWoAzY0U4N5aXM2NZZLnYppmZmVlTcYNjZmZmLccNjpmZmbWcARscSVdmua/K7x4v6XFJSySd18/jH5W0UtJD6XRmtrKtkTkzVkTR3Dgzw5fXNVZLlks1TKmckdQGHD7QL6XL/RB4B7AUuE/SvIh4pM+i/xkRZ2es15qDM2NF5M6NMzPseV1jVVXdgiNptqS1wCGSXpa0Np1fAfwqw9hvBpZExFMR0QlcA5xcl6qtITkzVsQgc+PMDENe11gWVRuciLgwIsYBF0XE9hExLp12jojZGcaeBDxbMb80va+vD0haJGmupD36G0jSWZLul3T/ypUrMzy1DQVnxooYZG7qlhlwbpqF1zWWxYDH4ETEbEknSfp2Op2YcWz1c1/0mf8vYK+IOAT4LTCnSg2XRMS0iJi2yy67ZHx6GyrOjBVRMDd1y0xag3PTRLyusVqyHGR8IXAO8Eg6nZPeN5ClQGXHOxl4rnKBiHghIl5NZ39Chn2n1vicGSuiYG6cmWHM6xqrJctBxu8BpkZEN4CkOcAfgIE2A94H7CfpjcDfgFOBf6xcQNLEiFiWzp4EPJqjdmtczowVUSQ3zszw5nWNVZWlwQHYEei5eNkOWX4hIjZLOhu4GWgDLouIxZK+CtwfEfOAT0s6Cdicjv/RPMVbQ3NmrIhcuXFmDK9rrApF9N3t2GcB6TTgm8ACkv2WRwOzI+Ka8st7vWnTpsX9998/FE89HPS3Xzr/IM7McOPcWF7OjOWVOzM1t+BIEnAnMB04In2Cz0fE8kLlWctzZqwI58bycmZsIDUbnIgISddHxOHAvK1UkzUxZ8aKcG4sL2fGBpLlWlT3SDqi9EqslTgzVoRzY3k5M1ZVloOMjwE+IekZ4BWSzYCRnhvArD/OjBXh3FhezoxVlaXBOaH0KqzVODNWhHNjeTkzVtWADU5E/EXSYcBbSc70eFdEPFh6Zda0nBkrwrmxvJwZqyXLmYz/jeQU1TsDE4CfSfpS2YVZ83JmrAjnxvJyZqyWLLuoTgMOjYiNAJK+CTwIfK3MwqypOTNWhHNjeTkzVlWWb1E9A7RXzI8BniylGmsVz+DMWH7P4NxYPs/gzFgVWbbgvAoslnQryT7OdwB3SvoeQER8usT6rDk5M1aEc2N5OTNWVZYG57p06rGwnFKshTgzVoRzY3k5M1ZVlm9RzdkahVjrcGasCOfG8nJmrJYBGxxJRwHnA29Il+85kdLe5ZZmzcqZsSKcG8vLmbFasuyi+ilwLvAA0FVuOdYinBkrwrmxvJwZqypLg7MmIm4svRJrJc6MFeHcWF7OjFWVpcFZIOki4FqSI9YB8NkirQZnxopwbiwvZ8aqytLgvCX9Oa3ivgCOrX851iKcGSvCubG8nBmrKsu3qI7ZGoVY63BmrAjnxvJyZqyWqg2OpM/0uSuAVcCdEfF0qVVZU3JmrAjnxvJyZiyLWpdqGNdn2p5kM+CNkk7dCrVZ83FmrAjnxvJyZmxAVbfgRMRX+rtf0njgt8A1ZRVlzcmZsSKcG8vLmbEsslxscwsR8SLJyZTMMnFmrAjnxvJyZqxS7gZH0rHA6hJqsRblzFgRzo3l5cxYpVoHGf+J5MCtSuOB54DTyyzKmpMzY0U4N5aXM2NZ1Pqa+Il95gN4ISJeKbEea27OjBXh3FhezkyL6ejoYOHChcycOZMZM2bUZcxaBxn/pS7PYMOGM2NFODeWlzPTWubPn8/xxx9PRDB69Gjmz59flyYn9zE4ZmZmZvVw6623csopp7B582a6urro7Oxk4cKFdRnbDY6ZVXXjjTfyqU99io6OjqEuxcxayOrVq/nYxz7GO9/5TsaNG8eYMWNoa2tj9OjRzJw5sy7P4QbHzLawZs0a5syZw/Tp03n3u9/ND37wA2bNmuUmx8zq4rrrruOggw5izpw5zJ49myeeeIIFCxZwwQUX1G33FGS72KaZtbj169dzww03cPXVV/Ob3/yGzs5OdtxxRyQREb2bjeu14jGz4Wf58uV86lOfYu7cuUydOpVf//rXHHbYYQDMmDGj7usXNzhmw0zPtxWOPPJI1qxZwzXXXMO8efN45ZVXmDhxIv/yL//CqaeeSnd3N8cddxydnZ113WxsZsNLRHDFFVdw7rnnsn79er7xjW/wuc99jlGjRpX6vKU2OJKOB/4DaAMujYhv9nl8DHAFcDjwAvDBiHimzJqGszK+hldvzkx5NmzYwC9+8QvOOussNm3aRERyGpHx48fzoQ99iNNOO423ve1ttLW19f7O/PnzGz4z4Nw0kmZYz4AzszV0dHRw3XXXcfvtt/P73/+eo446iksvvZQDDzxwqzx/aQ2OpDbgh8A7gKXAfZLmRcQjFYt9DFgdEfumF0j7FvDBeteS5x9cKyy7efNmVq5cyYoVK3j++edZsWIF9957Lz/+8Y/p7u5mzJgxdd3PWS+NlBnI/jk0Wg7+/u//nscee4xHHnlki+mpp57qbWoAJHH66afzk5/8pOr/pMrYbFxvjZSbRstCvZbtb7n169ezatUqVq5cyapVq1i1ahX33XcfP/zhD+nq6qK9vb0h1zPgzNR72VdffZWXXnqJ1atX9/689957+drXvsbmzZsB+MxnPsNFF13EiBFb79DfMrfgvBlYEhFPAUi6BjgZqAzQycD56e25wA8kKSrXwoPU0dHBMcccQ2dnJ21tbZxyyinsvvvu/S773HPPMXfuXLq6unIv+4EPfKDmsr/85S8LLfu+972P3XbbjYh43bRs2TJuuOEGurq6GDFiBHvuuSdr167lhRdeqPmeNPDxFA2RGYB58+bxgQ98YMAsDCYz73//+9l99917m47Kl/Dcc89x/fXX9y574oknsuuuu9Ld3d07RQTd3d08//zzzJ8/n66urt5jZnqMGjWK/fffn8MOO4wPf/jDjBw5kq9//ets2rSJ0aNH8/GPf7z0zcRbQUPkpqOjg6OPPrpQFspa9v3vfz8TJ04E6F1v9NxetmwZv/rVr3rXHyeccAI777xz79d1e6aVK1dy11130d3djSR23XVX1q5dy/r162u+Hw28noEGyszMmTPZtGlTqX9zetY11Za99tpre5d9z3ve05uDniz03F65ciX33HNPbxbGjx/P+vXr2bBhQ83X2dbWxoQJE7ZqcwOgOv9deG1g6RTg+Ig4M53/CPCWiDi7YpmH02WWpvNPpsus6jPWWcBZ6ezBwMM5StkNmFQxH7z+FN/w2gXalGHZnuWyLFuPcbv7zMNr34CrTMxGYC2wGdiUTj23RwP7pmMH8ATQ96yf7RFxcJW6StdAmQHYA9i1Yr6/z6zen23f5SqX7Qa6+vxuj7Z06vEysJIkD69WLDsBWAVsC4wjyUo9zvzq3CSGcl3T8+86a8Z6fqdy/dGVTj1jB6/lqrILXs9r65me9UvP7THAPtRez4Az02NrZibvuibY8jmCZKNItSz05Gdb4EWSvzl7MXAWssqfmf62DNRjAv47yX7NnvmPAN/vs8xiYHLF/JPAzgOMe3/OOmaQfAib0p8zqo2bddlmHLdi7Nm1xiwrD82UmayfQ6N8tnnGLelzc26G+PPtqbWMceu9nnFmGiMzZWcxaxZyvFe5M1PmLqqlJP8L7jGZ5EJo/S2zVNJIYAeSzq9uIqJD0ixgJrAwIqqezKOVl+1ZHmjkk5k0RGYg+3vbCJ9t3hy0oIbITSN8vmUs24LrGXBmSl+2Z3mGMguD7apqdFsjgaeAN5JsqvojMKXPMp8ELk5vnwr8oowurqzusNXGLatWZ8bjOjfljttMtTozjfN+tXoWS9uCExGbJZ0N3EyyH/eyiFgs6atpofOAnwJXSlpC0hmfmmHoS0oq2eOWV2smzozHLcK5KW3MZhw3E2em1HEbptbSDjI2MzMzGyq+FpWZmZm1HDc4ZmZm1nKapsGRtIekBZIelbRY0jl1HLtN0h8k3VDHMXeUNFfSY2nNdTnblaRz09f/sKSrJbUXHOcySSvScz303Dde0q2S/pz+3KkeNQ+VMjOTjt8UuXFmsnNmthjXucnIf596x22ozDRNg0NyIqHPRsR/A6YDn5R0UJ3GPgd4tE5j9fgP4KaIOBB4Uz3GlzQJ+DQwLZITHrWR7cC3/lwOHN/nvvOA+RGxHzA/nW9mZWYGmiA3zkxuwz4z4NwU4L9PDZiZpmlwImJZRDyY3l5L8oFMqv1bA5M0GXgPcOlgx6oYc3vgaJKj8ImIzoh4qU7DjwTGKjkvwza8/twNmUTE7bz+nA4nA3PS23OAfyhaZCMoKzPQdLlxZjJyZrbg3GTkv0+9GiozTdPgVJK0F3Ao8Ps6DPdd4F+pfjrzIvYmOVX+z9JNi5dK2nawg0bE34BvA38FlgFrIuKWwY5b4e8iYln6XMvY8lIFTa3OmYEmyY0zU9xwzQw4N4Phv0+Nk5mma3AkbQf8EvifEfHyIMc6EVgREQ/UpbjXjAQOA34cEYeSXH9j0Jtg032OJ5OcnGp3YFtJHx7suK2unplJx2ua3DgzxQznzIBzU5T/PjVWZpqqwZE0iiQ8P4+Ia+sw5FHASZKeAa4BjpV0VR3GXQosjYieDn4uSaAG6zjg6YhYGRGbgGuBI+swbo/nJU0ESH+uqOPYQ6KEzEBz5caZycmZAZyb3Pz3qfEy0zQNjiSR7DN8NCL+vR5jRsTsiJgcEXuRHAx1W0QMuuOMiOXAs5IOSO+aBTwy2HFJNv1Nl7RN+n7Mor4Hn80DzkhvnwH8qo5jb3VlZAaaLjfOTA7OTC/nJgf/fQIaMTNRx2tFlDkBbyW55Poi4KF0encdx58J3FDH8aaSXKl1EXA9sFOdxv0K8BjwMHAlMKbgOFeT7CfdRNLRfwzYmeTo9D+nP8cP9efeyJlpltw4M86Mc9PcuXFmimXGl2owMzOzltM0u6jMzMzMsnKDY2ZmZi3HDY6ZmZm1HDc4ZmZm1nLc4JiZmVnLcYNTMkkLJU2rmN+r8gqpZn05M5aXM2NFtHpu3OCYmZlZy3GDUydp5/uYpDmSFkmaK2mboa7LGpczY3k5M1bEcM2NT/RXJ+kVZJ8G3hoRd0m6jOT01ycCE4EN6aKjge6IOHgo6rTG4cxYXs6MFTFcc+MtOPX1bETcld6+iuT03QAfioipETEVePfQlGYNypmxvJwZK2LY5cYNTn313RzmzWM2EGfG8nJmrIhhlxs3OPW1p6QZ6e3TgDuHshhrCs6M5eXMWBHDLjducOrrUeAMSYuA8cCPh7gea3zOjOXlzFgRwy43Psi4TtKDuG5olYOzrHzOjOXlzFgRwzU33oJjZmZmLcdbcMzMzKzleAuOmZmZtRw3OGZmZtZy3OCYmZlZy3GDY2ZmZi3HDY6ZmZm1HDc4ZmZm1nLc4JiZmVnLcYNjZmZmLccNjpmZmbUcNzhmZmbWctzglEDSYkkzqzw2U9LSrVySNRhnxIpwbiyv4ZyZYd3gSHpG0gZJ6yQtl3S5pO0GO25ETImIhXWoaZ2kWyoeGyPpO5Kek7Ra0o8kjRpsvVZdg2bkAkl/krRZ0vn9PP6Pkv4i6RVJ10saX/HY2ZLul/SqpMsLvwCrqdVyU7HMfpI2SrqqSA1WXTNmpmK5n0kKSftW3Leuz9Ql6ftF6ihqWDc4qfdGxHbAVOBQYPYQ1wNpTen0zor7zwOmAQcD+wOHAV8aigKHmUbLyBLgX4Ff931A0hTg/wIfAf4OWA/8qGKR54CvAZeVX+aw10q56fFD4L4SaxzumiYzPSS9Fdin7/0Vf8O2I8nUBuD/K6vQ/rjBSUXEcuBmkmABvVtMvi3pr5Kel3SxpLHpYxMk3SDpJUkvSrpD0oj0sWckHZfeHpt24qslPQIcMYgy3wt8LyJejIiVwPeAfxrEeJZDo2QkIuZExI3A2n4e/hDwXxFxe0SsA/438H5J49LfvTYirgdeGPQbYpm0Qm7S5zsVeAmYP5j3wwbWJJlB0kjg+8DZA7ykU4AVwB2Z3oA6cYOTkjQZOIGkY+3xLZItJVOBfYFJwL+lj30WWArsQtKdfgGIfob+Mkl3uw/wLuCMDOX8XNJKSbdIelNlmelUOT9Z0g4ZxrRBarCMVDMF+GPPTEQ8CXSmNdoQaIXcSNoe+Gpam5WsSTIDcC5we0QsGmC5M4ArIqK/mkrjBgeul7QWeJakw/wygCQB/wycm24xWQt8Azg1/b1NwETgDRGxKSLuqPLh/T/A19MxniXZ6lLLh4C9gDcAC4CbJe2YPnYjcI6kXSTtBnw6vX+b3K/a8mi0jNSyHbCmz31rgHH9LGvlaqXcXAD8NH0eK0/TZEbSHsDHea3JqrbcnsDbgTlFn6soNzjwDxExDpgJHAhMSO/fhaRxeCDd7PcScFN6P8BFJN31LZKeknRelfF3Jwlrj7/UKiYi7oqIDRGxPiIuJNkk/Lb04a8DfwAeAu4GricJ9oqsL9YKaaiMDGAdsH2f+7anyiZmK1VL5EbSVOA44DuDGN+yaabMfBf4akT0bYz7Oh24MyKeHsRzFeIGJxURvwMuB76d3rWK5KCoKRGxYzrtkB4wRUSsjYjPRsTeJMfGfEbSrH6GXgbsUTG/Z97SSHdLpY3P2RExKX3eF4AHIqIr55hWQANnpNJioHe3pqS9gTHAE4MY0wahBXIzk2Sr8l8lLQc+B3xA0oODeD6roUkyMwu4SMk3vpan93VI+sc+y53OEGy9ATc4fX0XeIekqRHRDfwE+I6kXQEkTZL0rvT2iZL2TTcdvgx0pVNfvwBmS9op3a/6qWpPLmlPSUdJGi2pXdL/Iung76p4/t2VmE5yIOCX6/bqLYshzUg67ihJ7ST/fkemWWlLH/458F5Jb5O0LclxE9emm7SRNDL93TagLf3dkYN6RyyLZs7NJSTHbExNp4tJvlXzruJvh2XQ6JnZn6Qp7skFJM3VdRW/fyTJsUJb9dtTPdzgVEi/mXQFSeMA8HmSzX73SHoZ+C1wQPrYfun8OqAD+FGVcw18hWQz4NPALcCVNUoYB/wYWA38DTgeOCEier7xsg/JrqlXSDri8yLilv4GsnI0QEYgWdFtAE4Dvpje/kha32LgEyR/sFaQZOr/rfjdL6XLnwd8OL3tUw2UrJlzk+4uX94zpXVtTF+TlaQJMrOiTy4AVkXEhorfP4OK/2BtbdrKBzWbmZmZlc5bcMzMzKzllNbgSLpM0gpJD1d5XJK+J2mJpEWSDiurFmsezo3l5cxYXs7M8FDmFpzLSY4hqeYEkv2G+wFnkRx7YnY5zo3lcznOjOVzOU2YGUkzJM2WNKOey5ZlqOst7dsTEXG7pL1qLHIyr53Z8B5JO0qaGBHLyqrJGp9zY3k5M5ZXM2Um/WbUHiQH955P8g3Ibkm/IfkySmc6bar4uRvJSfjagE5J74iIO+tUywkk50V6CPgzMBZoT39W3t4/rWEk0CVpLrASGNXPtCvw1vRpXpU0KyI6BlvvUH49dBJbnnBoaXrf6wIk6SySLpqDDjro8MWLF2+VAochDbzIkMuUG2dmq2r03Hhd03icmVRHRwcLFy5k5syZzJgxg1deeYUHHniAe+65h3vuuYeJEyeybNnrnrZt++23f297ezudnZ10dnayadMmNm3a1N9TtLe1td1x+OGHc8ghh/ROb3rTm5gwYUK/Nbz66qssWbKExx9/nMcee4zHH3+cx4cEKnoAABNuSURBVB9/nG233ZZXXnkl1+tLjRw9evSp2267LaNGjeqdRo4cyahRo1i9ejXLlydfxGpraxt7wQUX3N3PGLkzM5QNTn/F9vuVroi4hORcDEybNs1f+xreMuXGmbEKXtdYXlslMx0dHRx77LF0dnYyYsQI9t57b5588km6upJT2Oy7777MmjWL6dOns8022/DJT36Szs5ORo8ezU033cSMGVvuzYkINm/ezB133MGJJ55IZ2cnbW1tnHLKKaxatYqbbrqJyy+/vHf5iRMnsueee/LAAw/Q1dWFpN6Gqru7u3e5SZMmccABB3DwwQdz7733EhGMGDGC008/nY9+9KO0t7czduzY3qm9vZ1Fixb11jB69Gjmz5//unor34dZs2b1Ljtz5sw8b2NVQ9ngLGXLMypOBp4bolqseTg3lpczY3ltlcwsXLiQV199lYigu7ubrq4uZs+ezfTp03nzm9/MLrvsssXyBx544BZbWvqSxKhRozj22GOZP39+v8uuWLGCRYsW9U633HILmzdvBpIGafz48fzTP/0TBx54IAcccAD7778/48YllyTr24icddZZVZuWWjX0NWPGjMzL5hIRpU0kp/d+uMpj7yG5eKSA6cC9WcY8/PDDw0pTah6yTvXOjTNTupbLTDg3ZXNmIuLuu++O9vb2aGtri7Fjx8bdd9+d6/fr4e67746xY8dmruHuu++Ob3zjG0NRa+7PuLQtOJKuJrmGyQRJS0kuKTAKICIuBn4DvJvkzIzrgf9RVi3WPJwby8uZsbwaJTMzZszgtttuq/+Wi5w15Nl6MmPGjCGps4imO5PxtGnT4v777x/qMlpVox/4V4gzUzrnxvJyZiyv3JnxmYzNzMys5bjBMTMzs5bjBsfMzMxajhscMzMzazlucMzMzKzluMExMzOzluMGx8zMzFqOGxwzMzNrOQM2OJJulbRjxfxOkm4utyxrZs6MFeHcWF7OjNWSZQvOhIh4qWcmIlYDu5ZXkrUAZ8aKcG4sL2fGqsrS4HRL2rNnRtIbqHLZeLOUM2NFODeWlzNjVWW52OYXgTsl/S6dPxo4q7ySrAU4M1aEc2N5OTNW1YANTkTcJOkwkkvGCzg3IlaVXpk1LWfGinBuLC9nxmrJsgUHoAtYAbQDB0kiIm4vryxrAc6MFeHcWF7OjPVrwAZH0pnAOcBk4CGSTrkDOLbc0qxZOTNWhHNjeTkzVkuWg4zPAY4A/hIRxwCHAitLrcqanTNjRTg3lpczY1VlaXA2RsRGAEljIuIx4IByy7Im58xYEc6N5eXMWFVZjsFZmp5I6XrgVkmrgefKLcuanDNjRTg3lpczY1Vl+RbV+9Kb50taAOwA3FRqVdbUnBkrwrmxvJwZq6VmgyNpBLAoIg4GiIjf1VrezJmxIpwby8uZsYHUPAYnIrqBP1aeKdKsFmfGinBuLC9nxgaS5RicicBiSfcCr/TcGREnlVaVNTtnxopwbiwvZ8aqytLgfKX0KqzVODNWhHNjeTkzVlXVBkfS9Ii4x/s1LStnxopwbiwvZ8ayqHUMzo96bkjq2Aq1WPNzZqwI58bycmZsQLUaHFXcbi+7EGsJzowV4dxYXs6MDajWMTgjJO1E0gT13O4NVUS8WHZx1nScGSvCubG8nBkbUK0GZwfgAV4LzYMVjwWwd1lFWdNyZqwI58bycmZsQFUbnIjYayvWYS3AmbEinBvLy5mxLLJcbNPMzMysqbjBMTMzs5ZTaoMj6XhJj0taIum8fh7/qKSVkh5KpzPLrMcanzNjeTkzVoRz0/oGbHAkXZnlvn6WaQN+CJwAHAScJumgfhb9z4iYmk6XZqjZGpwzY0UUyY0zM7x5XWO1ZNmCM6VyJg3G4Rl+783Akoh4KiI6gWuAk/OXaE3ImbEiiuTGmRnevK6xqqo2OJJmS1oLHCLpZUlr0/kVwK8yjD0JeLZifml6X18fkLRI0lxJe1Sp5SxJ90u6f+XKlRme2oaCM2NFDDI3dctMWotz0wS8rrEsqjY4EXFhRIwDLoqI7SNiXDrtHBGzM4ytfu6LPvP/BewVEYcAvwXmVKnlkoiYFhHTdtlllwxPbUPBmbEiBpmbumUmrcW5aQJe11gWA15NPCJmSzoJODq9a2FE3JBh7KVAZcc7GXiuz9gvVMz+BPhWhnGtwTkzVkTB3Dgzw5jXNVZLloOMLwTOAR5Jp3PS+wZyH7CfpDdKGg2cCszrM/bEitmTgEezFm6Ny5mxIgrmxpkZxryusVoG3IIDvAeYGhHdAJLmAH8Aam4GjIjNks4GbgbagMsiYrGkrwL3R8Q84NNp970ZeBH4aOFXYo3EmbEicufGmRn2vK6xqrI0OAA7knzAkFwDJJOI+A3wmz73/VvF7dkMEERrWs6MFZE7N87MsOd1jfUrS4NzIfAHSQtIDsw6Gn/oVpszY0U4N5aXM2NV1WxwJAm4E5gOHEESoM9HxPKtUJs1IWfGinBuLC9nxgZSs8GJiJB0fUQcTp8DsMz648xYEc6N5eXM2ECynMn4HklHlF6JtRJnxopwbiwvZ8aqynIMzjHAJyQ9A7xCshkw0pMfmfXHmbEinBvLy5mxqrI0OCeUXoW1GmfGinBuLC9nxqrKcibjv0g6DHgryams74qIB0uvzJqWM2NFODeWlzNjtWQ5k/G/kVyDY2dgAvAzSV8quzBrXs6MFeHcWF7OjNWSZRfVacChEbERQNI3gQeBr5VZmDU1Z8aKcG4sL2fGqsryLapngPaK+THAk6VUY63iGZwZy+8ZnBvL5xmcGasiyxacV4HFkm4l2cf5DuBOSd8DiIhPl1ifNSdnxopwbiwvZ8aqytLgXJdOPRaWU4q1EGfGinBuLC9nxqrK8i2qOVujEGsdzowV4dxYXs6M1TJggyPpKOB84A3p8j0nUtq73NKsWTkzVoRzY3k5M1ZLll1UPwXOBR4Ausotx1qEM2NFODeWlzNjVWVpcNZExI2lV2KtxJmxIpwby8uZsaqyNDgLJF0EXEtyxDoAPluk1eDMWBHOjeXlzFhVWRqct6Q/p1XcF8Cx9S/HWoQzY0U4N5aXM2NVZfkW1TFboxBrHc6MFeHcWF7OjNVStcGR9Jk+dwWwCrgzIp4utSprSs6MFeHcWF7OjGVR61IN4/pM25NsBrxR0qlboTZrPs6MFeHcWF7OjA2o6haciPhKf/dLGg/8FrimrKKsOTkzVoRzY3k5M5ZFlottbiEiXiQ5mZJZJs6MFeHcWF7OjFXK3eBIOhZYXUIt1qKcGSvCubG8nBmrVOsg4z+RHLhVaTzwHHB6mUVZc3JmrAjnxvJyZiyLWl8TP7HPfAAvRMQrJdZjzc2ZaTEdHR0sXLiQmTNnMmPGjLKexrmxvJwZG1Ctg4z/sjULsebnzLSWjo4Ojj32WDo7OxkzZgzz588vpclxbiwvZ8ayyH0Mjpm1vo0bN/LFL36RjRs30t3dTWdnJwsXLhzqsszMMnODY2ZbuO222zjkkENYsGABbW1ttLW1MXr0aGbOnDnUpZmZZZblWlRmNgysWrWKz33uc8yZM4d99tmHW265he22225rHINjZlZ3bnDMhrmI4IorruCzn/0sa9as4Qtf+AJf+tKXGDt2LIAbG8ulo6ODefPmcdJJJzk7NqTc4JgNY0888QSf+MQnWLBgAUceeSSXXHIJU6ZMGeqyrIlEBEuWLOH2229n7ty53HzzzUQE3/3ud7ntttvc5NiQKfUYHEnHS3pc0hJJ5/Xz+BhJ/5k+/ntJe5VZjzU+Z6Z8HR0dXHDBBfzzP/8zhxxyCA8++CAXX3wxd9xxR9M2N85NuTo6Orjwwgvp6OggInj00Ue5+OKLOe2005g8eTL7778/Z555JnfccQcRyelpNm3a1NAHpjszra+0LTiS2oAfAu8AlgL3SZoXEY9ULPYxYHVE7JteIO1bwAfrXUuec3l42aHTSJmB7O9XI3xeHR0dLFiwgBkzZjBlyhTWrVu3xbR27VrWrVvHokWL+P73v8+mTZsAmDVrFldddRW77bZbzfEbWSPlplGyUGvZiCAi6O7u5u677+a2227jiCOO4OCDD2bjxo1s2LBhi58PPfQQ559/Pps2bWLEiBFsv/32rF6dnCx44sSJvP3tb++dVq9ezXHHHUdnZ2dDH5jeSJmx8pS5i+rNwJKIeApA0jXAyUBlgE4Gzk9vzwV+IEnR81+AOujo6ODoo49m8+bNjBgxgkMOOYQddtih32XXrFnDokWL6O7ubtllI4L29vbSzmkySA2RGYArr7yS009/7YSoY8eOpa2t7XXLdXV1sWHDht759vb2fpfrWXbjxo2986NHj6atra33f7yVL6Grq4vNmzf3zre1tTFiRP8bXLu7u+nq6sr4yl4zYsQIZs2a1dTNTaohctPR0cFRRx3V+zlWywy8Pjd5lm1vb98iC31z09nZ2TvfM2ZPUzPYl9vV1cUb3vAGLrroIt7+9rezzz77IG156af58+c39H+kUg2RGSuXyvqsJJ0CHB8RZ6bzHwHeEhFnVyzzcLrM0nT+yXSZVX3GOgs4K509GHg4Rym7AZMq5jvTqa9RJBdpG51hWdLlsizbiOMGySnNl/dZpj0iDq7y+6VroMxAkpnKv/zrgL5nSR1Lspt3uwGW67FtjWUr/yFu08+4awcYd1zF/Evp1AV0V/zcAdgA7E2SnQCeqDFuVs5Nou+6ploW6pmbyjE39LNsT276rugjXW77ivtWk+SmO50ifXwDsFfF7zkzW4412HVNFhOAVQMu1RjjllVr/sxUdvb1nID/DlxaMf8R4Pt9llkMTK6YfxLYeYBx789ZxwxgPbAp/Tmj2rhZl23VcfO+t62amTzvV7N8tpXvQ7r87Fpj5nyvnJsh/nz7fLZljevMlLCuGcr3q4xxG6nWMndRLQX2qJifTLLVoL9llkoaSfI/zBfrWUREdEiaBcwEFkZEh5etvewQaojMQPb3qxE+r7yfbfp4I37+RTVEbhrh8y1zWZyZUtY1VqIyOq202xoJPAW8kWTXyB+BKX2W+SRwcXr7VOAXzdQdttq4ZdXqzHhc56bccZupVmemcd6vVs9iaVtwImKzpLOBm4E24LKIWCzpq2mh84CfAldKWkLSGZ+aYehLSirZ45ZXaybOjMctwrkpbcxmHDcTZ6bUcRum1tIOMjYzMzMbKr7YppmZmbUcNzhmZmbWcpqmwZG0h6QFkh6VtFjSOXUcu03SHyTdUMcxd5Q0V9Jjac11OeOVpHPT1/+wpKsltRcc5zJJK9JzPfTcN17SrZL+nP7cqR41D5UyM5OO3xS5cWayc2a2GNe5ych/n3rHbajMNE2DA2wGPhsR/w2YDnxS0kF1Gvsc4NE6jdXjP4CbIuJA4E31GF/SJODTwLRITnjURrYD3/pzOXB8n/vOA+ZHxH7A/HS+mZWZGWiC3DgzuQ37zIBzU4D/PjVgZpqmwYmIZRHxYHp7LckHMqn2bw1M0mTgPcClgx2rYsztgaNJjsInIjoj4qU6DT8SGJuel2EbXn/uhkwi4nZef06Hk4E56e05wD8ULbIRlJUZaLrcODMZOTNbcG4y8t+nXg2VmaZpcCopuarrocDv6zDcd4F/JTk1eb3sDawEfpZuWrxU0raDHTQi/gZ8G/grsAxYExG3DHbcCn8XEcvS51oG7FrHsYdUnTMDTZIbZ6a44ZoZcG4Gw3+fGiczTdfgSNoO+CXwPyPi5UGOdSKwIiIeqEtxrxkJHAb8OCIOJbluy6A3wab7HE8mOTnV7sC2kj482HFbXT0zk47XNLlxZooZzpkB56Yo/31qrMw0VYMjaRRJeH4eEdfWYcijgJMkPQNcAxwr6ao6jLsUWBoRPR38XJJADdZxwNMRsTIiNgHXAkfWYdwez0uaCJD+XFHHsYdECZmB5sqNM5OTMwM4N7n571PjZaZpGhxJItln+GhE/Hs9xoyI2RExOSL2IjkY6raIGHTHGRHLgWclHZDeNQt4ZLDjkmz6my5pm/T9mEV9Dz6bB5yR3j4D+FUdx97qysgMNF1unJkcnJlezk0O/vsENGJmoo7XiihzAt4KBLAIeCid3l3H8WcCN9RxvKkkVw1eBFwP7FSncb8CPAY8DFwJjCk4ztUk+0k3kXT0HwN2Jjk6/c/pz/FD/bk3cmaaJTfOjDPj3DR3bpyZYpnxpRrMzMys5TTNLiozMzOzrNzgmJmZWctxg2NmZmYtxw2OmZmZtRw3OGZmZtZy3OCUTNJCSdMq5veqvEKqWV/OjOXlzFgRrZ4bNzhmZmbWctzg1Ena+T4maY6kRZLmStpmqOuyxuXMWF7OjBUxXHPjE/3VSXoF2aeBt0bEXZIuIzn99YnARGBDuuhooDsiDh6KOq1xODOWlzNjRQzX3HgLTn09GxF3pbevIjl9N8CHImJqREwF3j00pVmDcmYsL2fGihh2uXGDU199N4d585gNxJmxvJwZK2LY5cYNTn3tKWlGevs04M6hLMaagjNjeTkzVsSwy40bnPp6FDhD0iJgPPDjIa7HGp8zY3k5M1bEsMuNDzKuk/Qgrhta5eAsK58zY3k5M1bEcM2Nt+CYmZlZy/EWHDMzM2s53oJjZmZmLccNjpmZmbUcNzhmZmbWctzgmJmZWctxg2NmZmYt5/8HhNNOrqFIl8YAAAAASUVORK5CYII=\n",
      "text/plain": [
       "<Figure size 576x288 with 8 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    }
   ],
   "source": [
    "plt.close('all')\n",
    "figrows = 2\n",
    "figcols = 4\n",
    "width = 8\n",
    "height = 4\n",
    "ylims = [0, 1]\n",
    "yticks = [0, 0.5, 1]\n",
    "xlims = [2, 10]\n",
    "xticks = [2, 4, 6, 8, 10]\n",
    "xlabel = 'pH'\n",
    "ylabel = 'Unprot Frac'\n",
    "\n",
    "#List residues that have pKa=nan\n",
    "nan_residue_list = [5, 15, 29, 33, 59, 101, 104, 147]\n",
    "\n",
    "fig, axes = plt.subplots(ncols = figcols, nrows = figrows, figsize = (width, height))\n",
    "flat_axes = axes.flatten()\n",
    "for i, axis in enumerate(flat_axes):\n",
    "    axis.spines['right'].set_visible(False)\n",
    "    axis.spines['top'].set_visible(False)\n",
    "    axis.set_ylim(ylims)\n",
    "    axis.set_yticks(yticks)\n",
    "    axis.set_xlim(xlims)\n",
    "    axis.set_xticks(xticks)\n",
    "    axis.set_xlabel(xlabel)\n",
    "    axis.set_ylabel(ylabel)\n",
    "    axis.set_title('Resid {}'.format(nan_residue_list[i]))\n",
    "    #Get S-values (unprotonated fractions) for residue i\n",
    "    s_values = [lambda_file.s[lambda_file.titrreses.index(nan_residue_list[i])]\n",
    "               for lambda_file in lambda_files]\n",
    "    axis.plot(single_phs, s_values, 'k-', marker = '.', clip_on = False)\n",
    "fig.tight_layout()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Apparently, residues 5, 15, 29, 33, 59, 101, and 104 have pKa's above our pH range (>9.3). Conversely, residue 147 appears to have a pKa below our pH range (<3.3)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
