from setuptools import setup

NAME = 'cphmd_tools'
VERSION = '0.1.0'

with open('README.md', 'r') as f:
    long_description = f.read()

with open('requirements.txt', 'r') as f:
    requirements = f.read().splitlines()

setup(
    name=NAME,
    version=VERSION,
    license="MIT",
    description='Analysis tools for CpHMD calculation results',
    long_description=long_description,
    author="Jana Shen Lab",
    url="http://computechem.org",
    author_email="jana.shen@rx.umaryland.edu",
    packages=[NAME],
    install_requires=requirements,
)
