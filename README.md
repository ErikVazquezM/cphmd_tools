# CpHMD Preparation/Analysis Tools

 A set of tools to help prepare and analyze
 **C**ontinuous Constant **pH** **M**olecular
 **D**ynamics (**CpHMD**) simulation outputs. CpHMD is developed by
 [Dr. Jana Shen's group](https://www.computchem.org/) at the
 University of Maryland School of Pharmacy.

 The python cphmdanalysis library  can be used to analyze the
 outputs of CpHMD simulations
 run with our CHARMM or Amber implementations of CpHMD.

 The processParm7 module can be used to change the GB radii
 of some of the atoms in your Amber parm7 file to the
 values required in CpHMD simulations.

## Requirements

* python >=3.4
* scipy >=1.3
* ParmEd >=3.2

## Installation

 If you use Git, choose a directory where you want to store the files, and
 run the following commands to install the library into your python
 installation,

   `git clone https://gitlab.com/rocharri/cphmd_tools.git`

   `cd cphmd_tools`

   `python setup.py install`

 Alternatively, you can download the zip/tarball directly. If you download
 the zip/tarball file, unzip/untar it first. For example, if you download
 a `.zip` file:

   `unzip cphmd_tools-master.zip -d cphmd_tools`

   `cd cphmd_tools`

   `python setup.py install`

 **Note: It is a good idea to create a python [virtual environment](https://docs.python.org/3/tutorial/venv.html)
   first to avoid conflicts with your other python projects.

## Test

### Test `cphmdanalysis` module

 We have implemented a test calculation to ensure that your installation
 of the cphmdanalysis library is working as expected. Running the
 following commands should run the test,

   `python tests/test_cphmdanalysis.py`

   `diff tests/test_results/test_single_pKa.txt examples/example_data/single_ph_lambdas/test_single_pKa.txt`

   `diff tests/test_results/test_REX_pKa.txt examples/example_data/rex_lambdas/test_REX_pKa.txt`

 You should see no error or output.

### Test `processParm7` module
 To test your preprocessParm7 module, run the following command,

   `python tests/test_processParm7.py`

 Compare the files with the same name in `tests/test_results/` and `examples/example_data/parm7`. If you see no difference, it's safe to use.

## Usage

 The cphmdanalysis library can be used to analyze the results of
 CpHMD single-pH and replica-exchange simulations and to compute
 the resulting pKa estimates.

 The jupyter notebooks `cphmdanalysis_single.ipynb`  and
 `cphmdanalysis_replica.ipynb` under `examples` provide
 examples of using the cphmdanalysis module to perform simple
 analyses of CpHMD simulations.

 The processParm7 module allows you to change the GB radii
 in your Amber parm7 files to values required in CpHMD simulations.
 The jupyter-notebook `tests/test_processParm7.py` provides
 an example of how to run this module.

To better understand pKa's calculated by CpHMD, check our recent related papers like [spotting protein kinase covalent inhibitor targets](https://pubs.acs.org/doi/10.1021/jacs.8b13248), [pH-responsive material design](https://pubs.acs.org/doi/full/10.1021/acs.chemmater.8b03753) and [GPU accelerated CpHMD](https://pubs.acs.org/doi/10.1021/acs.jcim.9b00754).
